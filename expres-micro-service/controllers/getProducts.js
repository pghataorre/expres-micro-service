function getProducts() {
    return [
      {
        "id": 0,
        "name": "Apples",
        "description": "Best red Apples for the best health and summer Apples Apples Apples",
        "price": 0.20,
        "image": "http://via.placeholder.com/350"
      }, {
        "id": 1,
        "name": "Bananas",
        "description": "Best ripe Bananas for the best health and summer Bananas Bananas Bananas",
        "price": 0.40,
        "image": "http://via.placeholder.com/350"
      }, {
        "id": 2,
        "name": "Oranges",
        "description": "Best ripe Oranges for the best health and summer Oranges Oranges Oranges",
        "price": 0.80,
        "image": "http://via.placeholder.com/350"
      }, {
        "id": 3,
        "name": "Kiwis",
        "description": "Best ripe Kiwis for the best health and summer Kiwis Kiwis Kiwis",
        "price": 0.80,
        "image": "http://via.placeholder.com/350"
      },  {
        "id": 4,
        "name": "Apricot",
        "description": "Best ripe Apricots for the best health and summer Apricots Apricots Apricots",
        "price": 0.10,
        "image": "http://via.placeholder.com/350"
      }
    ]
}

module.exports = getProducts
