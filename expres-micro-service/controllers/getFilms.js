const fetch = require('node-fetch');
const config = require('../config');

const getFilms = (searchValParam) => {
  const searchVal = searchValParam || 'beverly hills cop';

  return new Promise(function(resolve, reject) {
    let url = `${config.films.url}?api_key=${config.films.key}&language=${config.films.defatultLang}&query=${searchVal}&page=1&include_adult=${config.films.allowAdultContent}`

    return fetch(url)
    .then(response => {
      if (response.status < 200 && response.status > 300) {
        throw Error(response.statusText);
      }
      return response.json();
    })
    .then(response => {
      resolve(response);
    })
    .catch((err) => {
      reject({err})
    });
 })
}

module.exports = getFilms;
