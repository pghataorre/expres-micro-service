function getSecretWord() {
  return new Promise(function(resolve, reject) {
    const secretWords = [{
      "word": "Coffee", 
      "wordHints": [
        "A drink", 
        "Can be taken with milk and sugar"
      ]
    },
    {
      "word": "Mouse", 
      "wordHints": [
        "A small animal", 
        "has a tail and can be kept as a pet"
      ]
    },
    {
      "word": "Orange", 
      "wordHints": [
        "A fruit", 
        "a Citric fruit"
      ]
    },
    {
      "word": "Computer", 
      "wordHints": [
        "Requires a  a screen for use", 
        "Requires a keyboard for use"
      ]
    },
    {
      "word": "Telephone", 
      "wordHints": [
        "Ability to talk to people", 
        "Runs Andriod or Apple operating systems"
      ]
    },
    {
      "word": "Lighter", 
      "wordHints": [
        "Relates to smokers", 
        "Creates a flame"
      ]
    },
    {
      "word": "Television", 
      "wordHints": [
        "Used be only available in Black and White but now offer colour", 
        "Now created by LED, but was only available using a Tube"
      ]
    },
    {
      "word": "Speaker", 
      "wordHints": [
        "Used to play music", 
        "Can come in various sizes"
      ]
    },
    {
      "word": "Diamond", 
      "wordHints": [
        "A very expensive stone", 
        "A jewellery addition"
      ]
    },
    {
      "word": "Flowers", 
      "wordHints": [
        "Given to a person to show some form of affection", 
        "Grow in gardens"
      ]
    },
    {
      "word": "Neptune", 
      "wordHints": [
        "A planet in the galaxy", 
        "A blue coloured planet"
      ]
    },
    {
      "word": "Galaxy", 
      "wordHints": [
        "Many planets are found in one we are in the milky way", 
        "Huge collection of gas, dust, and billions of stars"
      ]
    },
    {
      "word": "Candle", 
      "wordHints": [
        "Come in may colours", 
        "Made out of wax"
      ]
    },
    {
      "word": "Skeleton", 
      "wordHints": [
        "Come in may colours", 
        "Made out of wax"
      ]
    }];

    resolve(secretWords);
  });
}

module.exports = getSecretWord;
