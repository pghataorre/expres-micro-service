const fetch = require('node-fetch');
const config = require('../config');

const getPics = (searchValParam) => {
  const searchVal = searchValParam || 'Space';

  return new Promise(function(resolve, reject) {
    let url = `${config.url}?key=${config.key}&q=${searchVal}&image_type=photo`

    return fetch(url)
    .then(response => {
      if (response.status < 200 && response.status > 300) {
        throw Error(response.statusText);
      }
      return response.json();
    })
    .then(response => {
      resolve(response);
    })
    .catch((err) => {
      reject({err})
    });
 })
}

module.exports = getPics;
