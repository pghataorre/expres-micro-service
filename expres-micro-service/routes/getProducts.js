var express = require('express');
var router = express.Router();
const getProducts = require('../controllers/getProducts');

router.get('/', function (req, res, next) {
    const response = getProducts();
    res.setHeader("Content-type", "application/json");
    res.json(response);
});

module.exports = router;
