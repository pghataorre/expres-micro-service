var express = require('express');
var router = express.Router();
const getFilms = require('../controllers/getFilms');

router.get('/', function (req, res, next) {
const searchVal = req.body.search;

  getFilms(searchVal)
    .then((response) => {
      res.setHeader("Content-type", "application/json");
      res.json(response);
    }).catch(function(err) {
      res.status(500);
      res.send(JSON.stringify({err}));
    });

});

module.exports = router;


