var express = require('express');
var router = express.Router();
const getPics = require('../controllers/getPics');

router.post('/', function (req, res, next) {
  const searchVal = req.body.search;

  getPics(searchVal).then((response) => {
    res.setHeader("Content-type", "application/json");
    res.json(response);
  }).catch(function(err) {
    res.status(500)
    res.send(JSON.stringify({err}));
  });

});

module.exports = router;
