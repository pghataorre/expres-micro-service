var express = require('express');
var router = express.Router();
const getFlightInfo = require('../controllers/getFlightInfo');

router.get('/', function (req, res, next) {
  const response = getFlightInfo();
  res.setHeader("Content-type", "application/json");
  res.json(response);
});

module.exports = router;
