var express = require('express');
var router = express.Router();
const getSecretWord = require('../controllers/getSecretWord');

router.get('/', function (req, res, next) {
getSecretWord()
  .then(function(response) {
    res.setHeader("Content-type", "application/json");
    res.json(response);
  }).catch(function(err) {
    res.status(500);
    res.send(JSON.stringify({err}));
  });
});

module.exports = router;
